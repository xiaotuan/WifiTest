package com.qty.wifitest;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class WifiTest extends Activity {

    private static final String TAG = "WifiTest";
    private static final int MSG_SCAN_WIFI = 0;
    private static final int MSG_UPDATE_WIFI_LIST = 1;
    private static final int MSG_STATE_CHANGED = 2;
    private static final int DELAYED_RESCAN_WIFI = 10000;

    private WifiManager mWifiManager;
    private TextView mWifiStateTv;
    private ListView mWifiScanResultsLv;
    private WifiAdapter mWifiAdapter;
    private TextView mEmptyView;

    private boolean mLastWifiEnabled = false;
    private boolean mNeedOpenWifiWhenDisabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_test);

        mWifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        mLastWifiEnabled = mWifiManager.isWifiEnabled();
        mWifiAdapter = new WifiAdapter(this, new ArrayList<ScanResult>());
        initEmptyView();

        mWifiStateTv = (TextView) findViewById(R.id.wifi_state);
        mWifiScanResultsLv = (ListView) findViewById(R.id.wifi_list);

        mWifiScanResultsLv.setAdapter(mWifiAdapter);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.gravity = Gravity.CENTER;
        ((ViewGroup) mWifiScanResultsLv.getParent()).addView(mEmptyView, lp);
        mWifiScanResultsLv.setEmptyView(mEmptyView);
        Log.d(TAG, "onCreate=>wifi last state: " + mLastWifiEnabled);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()...");
        openWifi();
        updateWifiState();
        registerWifiChangedReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()...");
        unregisterWifiChangedReceiver();
        closeWifi();
    }

    private void initEmptyView() {
        mEmptyView = new TextView(this);
        mEmptyView.setText(R.string.no_wifi_available_title);
        mEmptyView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        mEmptyView.setGravity(Gravity.CENTER);
    }

    private void updateWifiState() {
        int state = mWifiManager.getWifiState();
        Log.d(TAG, "updateWifiState=>state: " + state);
        switch (state) {
            case WifiManager.WIFI_STATE_ENABLING:
                mWifiStateTv.setText(R.string.wifi_openning_state_text);
                break;

            case WifiManager.WIFI_STATE_ENABLED:
                mWifiStateTv.setText(R.string.wifi_opened_state_text);
                startScanWifi();
                break;

            case WifiManager.WIFI_STATE_DISABLING:
                mWifiStateTv.setText(R.string.wifi_closing_state_text);
                break;

            case WifiManager.WIFI_STATE_DISABLED:
                mWifiStateTv.setText(R.string.wifi_closed_state_text);
                if (mNeedOpenWifiWhenDisabled) {
                    openWifi();
                    mNeedOpenWifiWhenDisabled = false;
                }
                break;

            default:
                mWifiStateTv.setText(R.string.wifi_unknow_state_text);
                break;
        }
    }

    private void registerWifiChangedReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(mWifiStateChangedReceiver, filter);
    }

    private void openWifi() {
        if (!mWifiManager.isWifiEnabled()) {
            Log.d(TAG, "openWifi=>state: " + mWifiManager.getWifiState());
            if (mWifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED){
                mWifiManager.setWifiEnabled(true);
            } else {
                mNeedOpenWifiWhenDisabled = true;
            }
        }
    }

    public void startScanWifi() {
        mWifiManager.startScan();
        mEmptyView.setText(R.string.scanning_wifi_title);
    }

    private void closeWifi() {
        if (!mLastWifiEnabled) {
            mWifiManager.setWifiEnabled(false);
        }
    }

    private void unregisterWifiChangedReceiver() {
        unregisterReceiver(mWifiStateChangedReceiver);
    }

    private void updateWifiList() {
        List<ScanResult> results = mWifiManager.getScanResults();
        Log.d(TAG, "updateWifiList=>scan results size: " + results.size());
        mWifiAdapter.setScanResultList(results);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SCAN_WIFI:
                    startScanWifi();
                    break;

                case MSG_UPDATE_WIFI_LIST:
                    updateWifiList();
                    mEmptyView.setText(R.string.no_wifi_available_title);
                    mHandler.sendEmptyMessageDelayed(MSG_SCAN_WIFI, DELAYED_RESCAN_WIFI);
                    break;

                case MSG_STATE_CHANGED:
                    updateWifiState();
                    break;
            }
        }
    };

    private BroadcastReceiver mWifiStateChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive=>action: " + action);
            if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
                mHandler.sendEmptyMessage(MSG_STATE_CHANGED);
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                mHandler.sendEmptyMessage(MSG_UPDATE_WIFI_LIST);
            }
        }
    };

    private class WifiAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private List<ScanResult> mList;

        public WifiAdapter(Context context, List<ScanResult> list) {
            mContext = context;
            mList = list;
            mInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        public void setScanResultList(List<ScanResult> list) {
            mList = null;
            mList = list;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (mList == null ? 0 : mList.size());
        }

        @Override
        public ScanResult getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.wifi_item, parent, false);
                holder = new ViewHolder();
                holder.mWifiSignal = (ImageView) convertView.findViewById(R.id.wifi_signal);
                holder.mWifiName = (TextView) convertView.findViewById(R.id.wifi_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            boolean encryptWifi = false;
            ScanResult info = getItem(position);
            Log.d(TAG, "getView=>name: " + info.SSID + " Level: " + info.level + " capabilities: " + info.capabilities);
            if (info.capabilities.toLowerCase().contains("wap") || info.capabilities.toLowerCase().contains("wep")
                    || info.capabilities.toLowerCase().contains("wpa")) {
                encryptWifi = true;
            }
            setWifiSignalImageView(holder.mWifiSignal, info.level, encryptWifi);
            holder.mWifiName.setText(info.SSID);
            return convertView;
        }

        private void setWifiSignalImageView(ImageView view, int level, boolean encrypt) {
            if (encrypt) {
                if (level <= 0 && level >= -50) {
                    view.setImageResource(R.drawable.ic_wifi_lock_signal_4_dark);
                } else if (level < -50 && level >= -70) {
                    view.setImageResource(R.drawable.ic_wifi_lock_signal_3_dark);
                } else if (level < -70 && level >= -80) {
                    view.setImageResource(R.drawable.ic_wifi_lock_signal_2_dark);
                } else if (level < -80 && level >= -100) {
                    view.setImageResource(R.drawable.ic_wifi_lock_signal_1_dark);
                } else {
                    view.setImageResource(R.drawable.ic_wifi_lock_signal_0_dark);
                }
            } else {
                if (level <= 0 && level >= -50) {
                    view.setImageResource(R.drawable.ic_wifi_signal_4_dark);
                } else if (level < -50 && level >= -70) {
                    view.setImageResource(R.drawable.ic_wifi_signal_3_dark);
                } else if (level < -70 && level >= -80) {
                    view.setImageResource(R.drawable.ic_wifi_signal_2_dark);
                } else if (level < -80 && level >= -100) {
                    view.setImageResource(R.drawable.ic_wifi_signal_1_dark);
                } else {
                    view.setImageResource(R.drawable.ic_wifi_signal_0_dark);
                }
            }
        }

        class ViewHolder {
            ImageView mWifiSignal;
            TextView mWifiName;
        }
    }
}
